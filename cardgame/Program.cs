﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardgame
{
    class Program
    {

        static void Main(string[] args)
        {
           GameManager game = new GameManager();
           game.game();
        }
    }

    class Card
    {
        #region parameters
        private int full_number;
        private int value;
        private string type;
        #endregion

        #region constractor
        public Card(int number)
        {
            // this function are makes a new deck of cards
            this.full_number = number;
            this.value = number / 4;
            if (full_number % 4 == 0)
                this.type = "spade";
            else if (full_number % 4 == 1)
                this.type = "heart";
            else if (full_number % 4 == 2)
                this.type = "diamond";
            else
                this.type = "club";
         }
        #endregion

        #region gets functions
        public int get_card_value()
        {
            // this function are return the card value
            return this.value;
        }
        
        public string get_card_type()
        {
            // this function are return the card type
            return this.type;
        }

        #endregion
            
        #region Tostring
        public override string ToString()
        {
            // this function return a sentence which describes about the card
            return "this card type is: " + get_card_type() + " and it's value is: " + get_card_value(); 
        }
        #endregion

        #region Ishigher 
        // this function states if the current card are higher or lower then the next one
        public bool IsHigher (Card C)
        {
            if (this.value > C.value)
                return true;
            if (this.value == C.value)
            {
                if (this.full_number % 4 > C.full_number % 4)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        #endregion
    }

    class Deck
    {
        private Card [] stcard = new Card[40];

        #region constractor
        public Deck ()
        {
            // Defines 40 cards
            for (int i = 0; i < 40; i++)
            {
                this.stcard[i] = new Card(i+4);
            }
            Console.WriteLine(this.stcard);
            shuffel_array();
         }
        #endregion

        #region shuffel
        public void shuffel_array()
        {
            var rand = new Random();
            // sheffel the deck
            for (int i = 0; i < stcard.Length; i++)
            {
                Card C1 = stcard[i];
                int ran = rand.Next(i, 40);
                stcard[i] = stcard[ran];
                stcard[ran] = C1;
            }
        }
        #endregion

        #region cards in deck
        public int cards_in_deck()
        {
            // this function return 
            int counter = 0;
            for (int i = 0; i < stcard.Length; i++)
                if (this.stcard[i] == null)
                    counter++;
            return 43 - counter;
        }
        #endregion

        #region get and delete the first card
        public Card get_and_delete_first_card()
        {
            // this function return the first card in the deck nd delete him
            for(int i =0; i< stcard.Length; i++)
            {
                if(stcard[i] != null)
                {
                    Card first = stcard[i];
                    stcard[i] = null;
                    return first;
                }
            }

            return new Card(0);
        }
        #endregion

        #region get the first card
        public Card get_first_card()
        {
            //this function only return the first card in the deck
            for (int i = 0; i < stcard.Length; i++)
            {
                if (stcard[i] != null)
                    return stcard[i];
                
            }
            return new Card(0);
        }
        #endregion
    }

    class GameManager
    {
        private Deck deck1 = new Deck();

        #region constractor
        public GameManager()
        {
            // this constractor are make a deck and shuffle it
            this.deck1.shuffel_array();
        }
        #endregion

        public string lower_or_higher()
        {
            // this function ask for a guess if card is higher or lower and returns the answer
            Console.WriteLine("GUESS, this card is Higher or Lower then the next one??");
            string guess = Console.ReadLine().ToLower();
            return guess;
        }

        #region game process
        public void game()
        {
            // this function are start a new game and she starts over and over again until the gaim is end.
            Card C1 = deck1.get_and_delete_first_card();
            Card C2 = this.deck1.get_first_card();
            Console.WriteLine("The top card is: {0}", C1.ToString());
            string guess = lower_or_higher();
            Console.WriteLine("The next card is {0}", C2.ToString());
            bool istrue = C1.IsHigher(C2);
            if ((istrue == true) && (guess == "higher") || (istrue == false) && (guess == "lower"))
            {
                Console.WriteLine("Congratulations!! you are right! you are on fire!!");
                game();
            }
            else
            {
                Console.WriteLine("Unfortunately, your guess is wrong and you are disqualified. you achived {0} correct answers!", 40 - this.deck1.cards_in_deck() - 1);
            }   
        }
        #endregion
    }
}

